# README #

RxTicTacToe is a reactive functional programming example that implements a simple game of Tic-Tac-Toe.
The core game logic is described as a series of Observable transformations on immutable data structures. The user interface then subscribes to this Observable stream and propagates changes from the player back to the game engine by implementing the core interfaces in whatever fashion it chooses. Concurrency is entirely in the hands of the client as well.