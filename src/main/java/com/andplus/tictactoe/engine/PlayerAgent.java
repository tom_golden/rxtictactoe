package com.andplus.tictactoe.engine;

import java.util.concurrent.Future;

import com.andplus.tictactoe.game.Game;
import com.andplus.tictactoe.game.Move;

/**
 * The {@link PlayerAgent} represents the interaction point between a {@link Referee} hosting a game and
 * some form of interaction on behalf of a player in the game. Implementations can be automated or require
 * human input.
 * 
 * @author Tom
 *
 */
public interface PlayerAgent {

  /**
   * Returns a {@link Future} that encapsulates a legal {@link Move} made by the player this agent represents.
   * 
   * @param game The current state of the game.
   * @return A move that can be applied to the current game state to result in an updated game board.
   */
  Future<Move> playMove(Game game);
  
}
