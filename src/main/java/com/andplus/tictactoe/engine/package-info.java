/**
 * The engine package contains classes that "glue" together the basic concepts from the game package to provide the
 * overarching logic - how turns are ordered, how moves are entered, etc.
 * 
 * @author Tom
 *
 */
package com.andplus.tictactoe.engine;