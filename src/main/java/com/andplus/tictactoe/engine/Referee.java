package com.andplus.tictactoe.engine;

import java.util.EnumMap;
import java.util.Map;

import com.andplus.tictactoe.game.Board;
import com.andplus.tictactoe.game.Game;
import com.andplus.tictactoe.game.Player;

import rx.Observable;
import rx.observables.ConnectableObservable;
import rx.subjects.PublishSubject;

/**
 * The {@link Referee} is responsible for running a game of Tic-Tac-Toe to its conclusion. She asks each
 * player to make their move in turn and determines when a game has ended.
 * 
 * @author Tom
 *
 */
public class Referee {

  private final Map<Player, PlayerAgent> agents = new EnumMap<>(Player.class);
  
  public Referee(final PlayerAgent forX, final PlayerAgent forO) {
    agents.put(Player.X, forX);
    agents.put(Player.O, forO);
  }
  
  /**
   * Creates a new {@link Observable} stream of {@link Game} states representing a full game of tic-tac-toe.
   * On subscription, each {@link PlayerAgent} is consulted for their moves in turn until the game is complete.
   * @return An observable stream of game events.
   */
  public Observable<Game> playGame() {
    final Game newGame = new Game(new Board(), Player.X);//X always goes first
    final PublishSubject<Game> gameSubject = PublishSubject.create();
    
    /*
     * We need to use a replay Observable to cache the game state pushed to our subject, 
     * because it won't be subscribed to by the zip operator until after the initial game
     * state is processed by doOnNext 
     */
    final ConnectableObservable<Game> latestGameState = gameSubject.replay(1);
    latestGameState.connect();
    
    final Observable<Player> turnOrder = Observable.just(Player.X, Player.O).repeat();
    
    return Observable.<Player, Game, Observable<Game>> zip(turnOrder, latestGameState, (player, game) -> Observable
        .from(agents.get(player).playMove(game))
        .map(move -> move.apply(game)))
      .flatMap(obs -> obs)
	    .startWith(newGame)
	    .takeUntil(Game::isOver)
	    .doOnNext(gameSubject::onNext);
  }
  
}
