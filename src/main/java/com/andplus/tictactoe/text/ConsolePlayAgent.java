package com.andplus.tictactoe.text;

import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import com.andplus.tictactoe.engine.PlayerAgent;
import com.andplus.tictactoe.game.Game;
import com.andplus.tictactoe.game.Move;
import com.andplus.tictactoe.game.Player;
import com.andplus.tictactoe.game.Position;

/**
 * The {@link ConsolePlayAgent} is a {@link PlayerAgent} that asks a human for input on the next
 * move by reading from stdin.
 * 
 * @author Tom
 *
 */
public final class ConsolePlayAgent implements PlayerAgent {

  private static final Position[][] COORDS = {
      {Position.TOP_LEFT, Position.TOP_CENTER, Position.TOP_RIGHT}, 
      {Position.MIDDLE_LEFT, Position.MIDDLE_CENTER, Position.MIDDLE_RIGHT}, 
      {Position.BOTTOM_LEFT, Position.BOTTOM_CENTER, Position.BOTTOM_RIGHT}
  };
  
	private final Player self;
	
	private final Scanner scanner;
	
	/**
	 * Creates a new {@link ConsolePlayAgent} for the given player, reading their input from the given scanner.
	 * @param self A {@link Player} this agent will represent.
	 * @param scanner A {@link Scanner} this agent will read user input from.
	 */
	public ConsolePlayAgent(final Player self, final Scanner scanner) {
		this.self = self;
		this.scanner = scanner;
	}
	
	@Override
	public Future<Move> playMove(Game game) {
		System.out.println("Enter the X,Y coordinates of your move");
		System.out.print("X: ");
		final int x = Integer.parseInt(scanner.nextLine());
		System.out.print("Y: ");
		final int y = Integer.parseInt(scanner.nextLine());
		//TODO Error handling of user input
		return CompletableFuture.completedFuture(new Move(self, COORDS[y - 1][x - 1]));//Handle zero-indexing
	}

}
