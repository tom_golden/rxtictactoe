package com.andplus.tictactoe.text;

import java.util.Scanner;

import com.andplus.tictactoe.engine.Referee;
import com.andplus.tictactoe.game.Game;
import com.andplus.tictactoe.game.Player;
import com.andplus.tictactoe.game.Position;

import rx.Observable;
import rx.Observer;

/**
 * The {@link ConsoleStadium} represents an entry point for playing Tic-Tac-Toe on the console against an AI opponent.
 * The AI is not very smart.
 * @author Tom
 *
 */
public final class ConsoleStadium {

  /**
   * Starts a game of Tic-Tac-Toe on the console.
   * The player will play as X, the AI as O.
   * @param args Ignored.
   */
	public static void main(String[] args) {
		try(final Scanner scanner = new Scanner(System.in)) {
			final Referee ref = new Referee(new ConsolePlayAgent(Player.X, scanner), new AutoPlayAgent(Player.O));
			final Observable<Game> game = ref.playGame();
			game.subscribe(new Observer<Game>() {
					@Override
					public void onCompleted() {
						System.out.println("Thanks for playing!");
					}
		
					@Override
					public void onError(Throwable arg0) {
						System.err.println("Ooops! We ran into a " + arg0.getMessage());
					}
		
					@Override
					public void onNext(Game arg0) {
					  System.out.println("[" + arg0.at(Position.TOP_LEFT).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.TOP_CENTER).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.TOP_RIGHT).map(Player::toString).orElse(" ") + "]");
					  System.out.println("[" + arg0.at(Position.MIDDLE_LEFT).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.MIDDLE_CENTER).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.MIDDLE_RIGHT).map(Player::toString).orElse(" ") + "]");
					  System.out.println("[" + arg0.at(Position.BOTTOM_LEFT).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.BOTTOM_CENTER).map(Player::toString).orElse(" ") + "]"
					          + "[" + arg0.at(Position.BOTTOM_RIGHT).map(Player::toString).orElse(" ") + "]");
					  
						if(arg0.isOver()) {
							System.out.println(arg0.winner().map(p -> "Player " + p + " wins!").orElse("Aw man, a stalemate!"));
						} else {
							System.out.println("The game's heating up now!");
						}
					}
					
				});
		}
		
	}

}
