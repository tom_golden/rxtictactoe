/**
 * The text package provides a console-based interface to the Tic-Tac-Toe game.
 * 
 * @author tom
 *
 */
package com.andplus.tictactoe.text;