package com.andplus.tictactoe.text;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import com.andplus.tictactoe.engine.PlayerAgent;
import com.andplus.tictactoe.game.Game;
import com.andplus.tictactoe.game.Move;
import com.andplus.tictactoe.game.Player;
import com.andplus.tictactoe.game.Position;

/**
 * The {@link AutoPlayAgent} is a very simple {@link PlayerAgent} that moves to the first open
 * space it finds, with no strategy whatsoever.
 * @author Tom
 *
 */
public final class AutoPlayAgent implements PlayerAgent {

	private final Player self;
	
	/**
	 * Creates a new {@link AutoPlayAgent}.
	 * @param self The {@link Player} this agent will represent.
	 */
	public AutoPlayAgent(final Player self) {
		this.self = self;
	}
	
	@Override
	public Future<Move> playMove(Game game) {
		//TODO Replace with MinMax tree
	  return Arrays.stream(Position.values())
	    .filter(pos -> !game.at(pos).isPresent())
	    .findFirst()
	    .map(pos -> CompletableFuture.completedFuture(new Move(self, pos)))
	    .orElseThrow(() -> new IllegalStateException("No legal moves available"));
	}

}
