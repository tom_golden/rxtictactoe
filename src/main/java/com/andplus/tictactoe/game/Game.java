package com.andplus.tictactoe.game;

import java.util.Optional;

import com.google.common.base.Preconditions;

/**
 * A {@link Game} encapsulates both the {@link Board} state and the current {@link Player}.
 * 
 * @author Tom
 *
 */
public class Game {

  private final Board board;
  
  private final Player currentPlayer;
  
  /**
   * Creates a new {@link Game} that encapsulates a {@link Board} and a current {@link Player} who will
   * make the next move.
   * @param board A board representing the latest state of all player moves.
   * @param currentPlayer A player who will move next, if a move is allowable.
   */
  public Game(final Board board, final Player currentPlayer) {
    this.board = board;
    this.currentPlayer = currentPlayer;
  }
  
  /**
   * Determines the winner of the game in its current state, if any.
   * If the game is not yet over or is a stalemate, returns an absent value.
   * @return An {@link Optional} {@link Player} who has won.
   */
  public Optional<Player> winner() {
    return board.winner();
  }
  /**
   * Determines the player who owns the space at the given position, if any.
   * If the space is still open, returns an absent value.
   * @param pos A {@link Position} to check on the game board.
   * @return An {@link Optional} {@link Player} who owns the space.
   */
  public Optional<Player> at(final Position pos) {
    return board.at(pos);
  }
  
  /**
   * Updates the game board by making a move for the current player.
   * @param player A {@link Player} who is making a move in the specified space.
   * @param pos A {@link Position} to move into.
   * @return An updated {@link Game} representing the effect of this move.
   */
  public Game makeMove(final Player player, final Position pos) {
    Preconditions.checkArgument(player == currentPlayer, "It's not your turn!");
    return new Game(board.makeMove(player, pos), currentPlayer == Player.X ? Player.O : Player.X);
  }
  
  /**
   * Determines if this game state will allow the provided player to make a move.
   * @param me A {@link Player} who would like to make a move.
   * @return True if that player can legally move this turn; false if it's not their turn.
   */
  public boolean isMyTurn(final Player me) {
    return me == currentPlayer;
  }
  
  /**
   * Determines if this game is over or not. A game can end when there is a winner or if no more moves can be
   * played on the board.
   * @return True if the game cannot continue, either because a player has won or there are no legal moves left; false if
   * play can continue.
   */
  public boolean isOver() {
    final Board.State state = board.state();
    return state == Board.State.GAME_WON || state == Board.State.STALEMATE;
  }
  
}
