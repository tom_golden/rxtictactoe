package com.andplus.tictactoe.game;

/**
 * The {@link Position} enumeration details all of the legal spaces a player may move to.
 * 
 * @author Tom
 *
 */
public enum Position {
  TOP_LEFT,
  TOP_CENTER,
  TOP_RIGHT,
  
  MIDDLE_LEFT,
  MIDDLE_CENTER,
  MIDDLE_RIGHT,
  
  BOTTOM_LEFT,
  BOTTOM_CENTER,
  BOTTOM_RIGHT
}
