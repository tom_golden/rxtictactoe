package com.andplus.tictactoe.game;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Preconditions;

/**
 * A {@link Board} is an immutable representation of a game state, represented by a 3x3 grid of spaces. 
 * Each space may be owned by a {@link Player} or may be empty.
 * 
 * @author Tom
 *
 */
public final class Board {
  
  private static final class Coordinate {
    private final int x;
    private final int y;
    
    private Coordinate(final int x, final int y) {
      Preconditions.checkArgument(x >= 0 && x < 3);
      Preconditions.checkArgument(y >= 0 && y < 3);
      this.x = x;
      this.y = y;
    }
  }

  private static final Map<Position, Coordinate> COORDS = new EnumMap<>(Position.class);
  
  static {
    COORDS.put(Position.TOP_LEFT, new Coordinate(0, 0));
    COORDS.put(Position.TOP_CENTER, new Coordinate(1, 0));
    COORDS.put(Position.TOP_RIGHT, new Coordinate(2, 0));
    
    COORDS.put(Position.MIDDLE_LEFT, new Coordinate(0, 1));
    COORDS.put(Position.MIDDLE_CENTER, new Coordinate(1, 1));
    COORDS.put(Position.MIDDLE_RIGHT, new Coordinate(2, 1));
    
    COORDS.put(Position.BOTTOM_LEFT, new Coordinate(0, 2));
    COORDS.put(Position.BOTTOM_CENTER, new Coordinate(1, 2));
    COORDS.put(Position.BOTTOM_RIGHT, new Coordinate(2, 2));
  }
  
  public enum State { CAN_STILL_PLAY, GAME_WON, STALEMATE };
  
  private final Player[][] spaces = new Player[3][3];
  
  /**
   * Queries this {@link Board} to determine which {@link Player}, if any, owns the space at the given position.
   * 
   * @param pos A {@link Position} identifying a space.
   * @return An {@link Optional} player; absent if no one has claimed the given position.
   */
  public Optional<Player> at(final Position pos) {
    final Coordinate coord = COORDS.get(pos);
    return Optional.ofNullable(spaces[coord.x][coord.y]);
  }
  
  public Board makeMove(final Player player, final Position pos) {
    Preconditions.checkArgument(!at(pos).isPresent(), "The chosen space is already taken");
    
    final Coordinate coord = COORDS.get(pos);
    final Board updated = new Board();
    Arrays.stream(Position.values())
      .map(COORDS::get)
      .forEach(c -> updated.spaces[c.x][c.y] = this.spaces[c.x][c.y]);
    updated.spaces[coord.x][coord.y] = player;
    
    return updated;
  }
  
  /**
   * Returns the winner of the game as represented by this {@link Board}, if there is one. If not, an absent value is returned.
   * 
   * @return An {@link Optional} {@link Player} who has made 3 spaces in a row, column, or diagonal.
   */
  public Optional<Player> winner() {
    //check rows
    for(int y = 0; y < 3; y++) {
      if(spaces[0][y] == spaces[1][y] && spaces[1][y] == spaces[2][y] && spaces[0][y] != null) {
        return Optional.of(spaces[0][y]);
      }
    }
    //check columns
    for(int x = 0; x < 3; x++) {
      if(spaces[x][0] == spaces[x][1] && spaces[x][1] == spaces[x][2] && spaces[x][0] != null) {
        return Optional.of(spaces[x][0]);
      }
    }
    //Check diagonals
    if(spaces[0][0] == spaces[1][1] && spaces[1][1] == spaces[2][2] && spaces[0][0] != null) {
      return Optional.of(spaces[0][0]);
    }
    if(spaces[0][2] == spaces[1][1] && spaces[1][1] == spaces[2][0] && spaces[0][2] != null) {
      return Optional.of(spaces[0][2]);
    }
    return Optional.empty();
  }
  
  /**
   * Returns the {@link State} of this board.
   * 
   * @return A state enumeration determining if the game has been won, is a stalemate, or can continue being played.
   */
  public State state() {
    return winner().map((p) -> State.GAME_WON)
        .orElseGet(() -> {
          for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
              if(spaces[x][y] == null) {
                return State.CAN_STILL_PLAY;
              }
            }
          }
          return State.STALEMATE;
        });
  }
  
}
