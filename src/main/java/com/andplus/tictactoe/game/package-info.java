/**
 * The game package contains all of the core logic for implementing Tic-Tac-Toe, independent of any particular
 * method of rendering the game board or interacting with the user.
 * 
 * @author Tom
 *
 */
package com.andplus.tictactoe.game;