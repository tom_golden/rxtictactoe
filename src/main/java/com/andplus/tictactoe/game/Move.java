package com.andplus.tictactoe.game;

/**
 * The {@link Move} type represents a player's intent to move into the space indicated by the provided position.
 * Moves can be applied to a game to produce an updated game.
 * 
 * @author Tom
 *
 */
public final class Move {

  private final Player player;
  
  private final Position position;
  
  /**
   * Creates a new {@link Move} that encapsulates the given {@link Player} and {@link Position} they would like to move to.
   * @param player A player who will own the space at the given position when this move is applied.
   * @param position A position to be moved into by the given player.
   */
  public Move(final Player player, final Position position) {
    this.player = player;
    this.position = position;
  }
  
  /**
   * Applies this {@link Move} to the given {@link Game}, returning the updated game state.
   * @param previousState A game board that this move will be applied to.
   * @return The updated game state.
   */
  public Game apply(final Game previousState) {
    return previousState.makeMove(player, position);
  }
  
}
