package com.andplus.tictactoe.game;

/**
 * The {@link Player} enumeration represents the two possible players in a game of Tic-Tac-Toe: X and O.
 * @author Tom
 */
public enum Player {
  X,
  O
}
